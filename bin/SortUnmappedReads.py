# -*- coding: utf8 -*-

import os, sys, datetime, re

debut_script = datetime.datetime.now()
erreur = False
erreurValue = []
listReadsByContings = {}
GiTaxID = {}
listTaxons = {}
listHeader = {}

#Gestion des paramètres
if len(sys.argv) != 2:
	print ("Usage: SortUnmappedReads.py <file.sam>")
else:
	file1 = sys.argv[1]
	if not os.path.isfile(file1):
		erreurValue.append("SAM file not found.")
		erreur = True

	if erreur:
		print ("ERREUR :")
		for element in erreurValue:
			print ("\t"+element)
	else:
		f1 = open(file1, "r");	
		output = open("unmappedReads.fasta", "w")
		for line in f1:
			if line[0] != '@':
				fields = line.split()
				if fields[2] == '*':
					output.write(">"+fields[0]+"\n"+fields[9]+"\n")
	output.close()
	f1.close()

duree = datetime.datetime.now() - debut_script
print ("execution time : "+str(duree))


