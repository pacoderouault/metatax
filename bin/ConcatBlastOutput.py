# -*- coding: utf8 -*-

import os, sys, datetime, shutil
import argparse

debut_script = datetime.datetime.now()
erreur = False
erreurValue = []
list_fichier = []
hits = {}

parser = argparse.ArgumentParser()
parser.add_argument('-l', dest='lenghtmin', default=0, help='Minimum length alignment. (default: %(default)s)')
parser.add_argument('-o', dest='output', default=0, help='output file name. (default: %(default)s)')
parser.add_argument('-1', dest='input1', help='input blast file name.')
parser.add_argument('-2', dest='input2', help='input blast file name.')
options = parser.parse_args()


if options.output:
	output = options.output
else:
	output = "outputBlastUsearch"

fileToCat = [options.input1, options.input2]
for in_file in fileToCat:
	if not os.path.isfile(in_file):
		erreurValue.append("file not found : "+in_file)
		erreur = True
	else:
		list_fichier.append(in_file)
if erreur:
	print ("ERROR :")
	for element in erreurValue:
		print ("\t"+element)
else:
	fichier_final = open(output, "w")
	for i in list_fichier:
		f = open(i, "r")
		for line in f:
			words = line.split("\t")
			header = words[0].rstrip()

			if not header in hits:
				pos_deb = min(int(words[6]), int(words[7]))
				pos_fin = max(int(words[6]), int(words[7]))
				tailleHit = pos_fin-pos_deb
				if tailleHit >= int(options.lenghtmin):
					fichier_final.write(line)
fichier_final.close()

duree = datetime.datetime.now() - debut_script
print ("execution time : "+str(duree))


