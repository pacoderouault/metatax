# -*- coding: utf8 -*-

import os
import sys
import datetime
import argparse


def checkFileExist(fname):
	"""
		Check the existence of a file.

		:param fname: the path to the file to checkFileExist
		:type fname: str
		:return: A boolean with true if the file existe, or false if the file doesn't exist.
		:rtype: bool
	"""

	try:
		f = open(fname,'r')
		f.close()
		return 1
	except:
		return 0


def convertListDivName(namesDiv):
	"""
		Convert the division names in list.
	"""
	divNames = []
	elmts = namesDiv.split('-')
	if not elmts:
		raise ValueError("Incorrect divisions name format. ex : VRL-PHG")
	for divName in elmts:
		divNames.append(int(divName))
	return divNames


def selectTaxID(divID, nodeFile):
	"""
		Select all the taxid for each divID
	"""

	f = open(nodeFile, 'r')
	taxID = []
	for line in f:
		if line.strip():
			cols = line.split('|')
			if int(cols[4].strip()) in divID:
				taxID.append(int(cols[0].strip()))
	f.close()
	return taxID


def selectGiTaxid(taxID, gitax_file, output):
	"""
		Create the gi_tax file filtered.
	"""
	f = open(gitax_file, 'r')
	o = open(output, 'w')
	for line in f:
		if line.strip():
			cols = line.split()
			if int(cols[1]) in taxID:
				o.write(line)
	f.close()
	o.close()

def __main__():

	timeStart = datetime.datetime.now()
	parser = argparse.ArgumentParser()
	parser.add_argument( '--div_id', required=True, help='Required - A list of division ID (ex \'3-9\' for viruses and phages. Open the division dmp to have this ids)')
	parser.add_argument( '--gitax_file', required=True, help='Required - The path to the gi_taxid.dmp file.')
	parser.add_argument( '--nodes_file', required=True, help='Required - The path to the nodes.dmp file.')
	parser.add_argument( '--out', help='The name of the out file.')
	options = parser.parse_args()

	filesNotFound = []
	for f in [options.gitax_file, options.nodes_file]:
		if not checkFileExist(f):
			filesNotFound.append(f)
	if filesNotFound:
		print(filesNotFound)
		# raise IOError("The following files are not found :")

	if options.out:
		outfile = options.out
	else:
		outfile = "gi_taxid_"+options.div_names

	divID = convertListDivName(options.div_names)
	taxID = selectTaxID(divID, options.nodes_file)
	selectGiTaxid(taxID, options.gitax_file, outfile)

if __name__ == "__main__": __main__()