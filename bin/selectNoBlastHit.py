# -*- coding: utf8 -*-

import os, sys, datetime

debut_script = datetime.datetime.now()
erreur = False
erreurValue = []
seq = ""

#Gestion des paramètres
if len(sys.argv) != 3:
	print ("Usage: SelectSeq.py <total reads> <blast outpout>")
else:
	file1 = sys.argv[1]
	file2 = sys.argv[2]

	if not os.path.isfile(file1):
		erreurValue.append("Reads files are not found.")
		erreur = True

	if not os.path.isfile(file2):
		erreurValue.append("Blast output is not found.");
		erreur = True

	if erreur:
		print ("ERROR :")
		for element in erreurValue:
			print ("\t"+element)
	else:
		f1 = open(file1, "r")
		f2 = open(file2, "r")

		output='.'.join(file1.split('.')[:-1])+"_noblasthit.fasta";
		reads_noblasthit = open(output, "w")
		dicf2 = {}
		for line2 in f2:
			words = line2.split()
			header = words[0]
			words.remove(header)
			dicf2[header] = []
			dicf2[header].extend(words)

		for line in f1:
			if line != "\n":
				if line[0] == '>':
					# Traitement de la séquence
					if seq != "":
						cle = header[1:].split()[0]
						if not cle in dicf2:
							reads_noblasthit.write(header)
							reads_noblasthit.write(seq)
					header = line
					seq = ""
				else:
					seq += line

		# traitement de la dernière seq du fichier
		cle = header[1:].split()[0]
		if not cle in dicf2:
			reads_noblasthit.write(header)
			reads_noblasthit.write(seq)
		reads_noblasthit.close()

# Calcul de la durée du script
duree = datetime.datetime.now() - debut_script
print ("execution time : "+str(duree))


