# -*- coding: utf8 -*-

import os, sys, datetime, re

debut_script = datetime.datetime.now()

erreur = False
erreurValue = []
listContigs = {}

#Gestion des paramètres
if len(sys.argv) != 2:
	print ("Usage: ReadsByContigs.py <file.sam>")
else:
	file1 = sys.argv[1]

	if not os.path.isfile(file1):
		erreurValue.append("The sam file is not found.")
		erreur = True

	if erreur:
		print ("ERROR :")
		for element in erreurValue:
			print ("\t"+element)
	else:
		f1 = open(file1, "r")

		output = open("ReadsByContigs", "w")

		for line in f1:
			if line[0] != '@':
				fields = line.split()
				if fields[2] != '*':
					if fields[2] in listContigs:
						listContigs[fields[2]] += 1
					else:
						listContigs[fields[2]] = 1
		
		for contig in listContigs:
			output.write(contig+"\t"+str(listContigs[contig])+"\n")

	output.close()
	f1.close()

duree = datetime.datetime.now() - debut_script
print ("execution time : "+str(duree))






