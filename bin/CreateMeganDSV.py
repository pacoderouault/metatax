# -*- coding: utf8 -*-

import os, sys, datetime, re
import argparse

debut_script = datetime.datetime.now()
erreur = False
erreurValue = []
listReadsByContings = {}
GiTaxID = {}
listTaxons = {}
listHeader = {}

#Gestion des paramètres
parser = argparse.ArgumentParser()
parser.add_argument('-l', dest='lenghtmin', default=0, help='Minimum length alignment. (default: %(default)s)')
parser.add_argument('-b', dest='BlastOutput', default=0, help='The blast file output')
parser.add_argument('-t', dest='GiTaxIDFile', default=0, help='The taxonomy file')
parser.add_argument('-r', dest='ReadsByContigFile', default=0, help='The reads by contigs file')
options = parser.parse_args()

if options.lenghtmin:
	lenhitMin = options.lenghtmin
else:
	lenhitMin = 0


f1 = open(options.BlastOutput, "r")
f2 = open(options.GiTaxIDFile, "r")
f3 = open(options.ReadsByContigFile, "r")
output_name = options.BlastOutput+"_Tax"
output = open(output_name, "w")
output2 = open("OutputMegan/FichiersDSV/MeganDSV", "w")
output3 = open("OutputMegan/FichiersDSV/MeganDSV_min10ReadsbyTaxon", "w")
output4 = open("OutputMegan/FichiersDSV/MeganDSV_min5ReadsbyTaxon", "w")

for line in f3:
	line = line.lstrip()
	line = line.rstrip()
	words = line.split("\t")
	listReadsByContings[words[0]] = words[1]

for line in f2:
	line = line.rstrip();
	words = line.split("\t")
	if not words[0] in GiTaxID:
		GiTaxID[words[0]] = words[1]

for line in f1:
	hit = line.split('\t')
	seqhit = hit[1]
	header = hit[0]

	pos_deb = min(int(hit[6]), int(hit[7]))
	pos_fin = max(int(hit[6]), int(hit[7]))
	taille_hit = pos_fin-pos_deb

	if not header in listHeader:
		listHeader[header] = 1

		if taille_hit > int(lenhitMin):
			rsltgi = re.search("gi\|([\d]+)", seqhit)
			if rsltgi:
				gi = rsltgi.group(1)

				if gi in GiTaxID:
					taxon = GiTaxID[gi]

					if header in listReadsByContings:
						nbReads = int(listReadsByContings[header])
					else:
						nbReads = 1

					if taxon in listTaxons:
						listTaxons[taxon] += nbReads
					else:
						listTaxons[taxon] = nbReads
					output.write(line.rstrip()+"\t"+taxon+"\n")

for taxon in listTaxons:
	output2.write(taxon+","+str(listTaxons[taxon])+"\n")
	if listTaxons[taxon] > 5:
		output4.write(taxon+","+str(listTaxons[taxon])+"\n")
	if listTaxons[taxon] > 10:
		output3.write(taxon+","+str(listTaxons[taxon])+"\n")

f1.close()
f2.close()
f3.close()
output.close()
output2.close()
output3.close()
output4.close()

duree = datetime.datetime.now() - debut_script
print ("execution time : "+str(duree))
