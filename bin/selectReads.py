# -*- coding: utf8 -*-

import os, sys, datetime, argparse

debut_script = datetime.datetime.now()
erreur = False
erreurValue = []
hits={}
seq = ""

#Gestion des paramètres
parser = argparse.ArgumentParser()
parser.add_argument('-r', dest='readsFile', help='The reads file')
parser.add_argument('-b', dest='BlastOutput', help='The output blast file.')
options = parser.parse_args()

if not os.path.isfile(options.readsFile):
	erreurValue.append("Reads file not found")
	erreur = True

if not os.path.isfile(options.BlastOutput):
	erreurValue.append("Blast output not found.")
	erreur = True

if erreur:
	print ("ERROR :")
	for element in erreurValue:
		print ("\t"+element)
else:
	f1 = open(options.readsFile, "r")
	f2 = open(options.BlastOutput, "r")
	output_name  = '.'.join(options.readsFile.split('.')[:-1])+"_SeqSelected.fasta"
	reads_selectionne = open(output_name, "w")
	blastResults = []

	for line in f2:
		words = line.split("\t")
		header = words[0].split()[0]
		if not header in hits:
			pos_deb = min(int(words[6]), int(words[7]))
			pos_fin = max(int(words[6]), int(words[7]));
			hits[header] = [int(pos_deb)-1, int(pos_fin)-1];
			blastResults.append([header+"_"+str(int(pos_deb)-1)+"-"+str(int(pos_fin)-1)]+map(str, words[1:]))
	f2.close()
	f2 = open(options.BlastOutput, "w")
	for e in blastResults:
		f2.write('\t'.join(e))
	f2.close()

	
	for line in f1:
		if line != "\n":
			if line[0] == '>':
				if seq != "":
					if header in hits:
						seq = seq[hits[header][0]:hits[header][1]]
						reads_selectionne.write(">"+header.rstrip()+"_"+str(hits[header][0])+"-"+str(hits[header][1])+"\n")
						reads_selectionne.write(seq+"\n")
				header = line.split()[0]
				header = header[1:]
				seq = ""
			else:
				seq += line.rstrip()
	reads_selectionne.close()
	f1.close()

duree = datetime.datetime.now() - debut_script
print ("execution time : "+str(duree))


