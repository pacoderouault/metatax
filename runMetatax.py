# -*- coding: utf-8 -*-
import argparse
import sys
import os
import re
import shutil
import tempfile
import gzip
import bz2
import datetime
import subprocess
import mimetypes
from inspect import currentframe, getframeinfo

def stop_err( msg ):
	sys.stderr.write( "%s\n" % msg )
	sys.exit()


def run_job (frameinfo, cmd_line, errorMess, silent = False):
	"""
		Run an program.

		:param frameinfo: From the inspect module. Allow to print the line number of the script in a error message.
		:param cmd_line: The command line to run.
		:type cmd_line: str
		:errorMess: A message error.
		:type errorM: str
		:param silent: Print or not the commande line in the standard output.
		:type silent: bool
		:return: void
	"""
	if not silent:
		print (cmd_line)
	try:
		tmp = tempfile.NamedTemporaryFile().name
		error = open(tmp, 'w')
		proc = subprocess.Popen( args=cmd_line, shell=True, stderr=error)
		returncode = proc.wait()
		error.close()
		error = open( tmp, 'rb' )
		stderr = ''
		buffsize = 1048576
		try:
			while True:
				stderr += str(error.read( buffsize ))
				if not stderr or len( stderr ) % buffsize != 0:
					break
		except OverflowError:
			pass
		error.close()
		os.remove(tmp)
		if returncode != 0:
			raise Exception(stderr)
	except Exception as e:
		print (e)
		raise Exception('Line : '+str(frameinfo.lineno)+' - '+errorMess)
		


def checkFileExist(fname):
	"""
		Check the existence of a file.

		:param fname: the path to the file to checkFileExist
		:type fname: str
		:return: A boolean with true if the file existe, or false if the file doesn't exist.
		:rtype: bool
	"""

	try:
		f = open(fname,'r')
		f.close()
		return 1
	except:
		return 0


def parseAppPaths(file):
	"""
		Parse the appPAth file and return a dictionnary containing the path of each application.

		:param file: The path to the file containing the path of each application.
		:type file: str
		:return: A dictionnary containing the path of each application
		:rtype: dict
	"""
	if not checkFileExist(file):
		raise IOError("The file "+file+" is not found.")
	else:
		f = open(file, 'r')
		appPath = {}
		appPath['python'] = ""
		appPath['bwa'] = ""
		appPath['sortmerna'] = ""
		appPath['ray'] = ""
		appPath['blastn'] = ""
		appPath['usearch'] = ""
		appPath['megan'] = ""
		appPath['xvfb'] = ""

		for line in f:
			if line.strip():
				elmnts = re.split("[=:]", line)
				if len(elmnts) != 2:
					raise ValueError("There is an error in the file : "+file+". Invalid path for "+elmnts[0])
				else:
					app = elmnts[0].strip()
					path = elmnts[1].strip()
					if not app in appPath:
						raise ValueError ('The program name :'+app+' is not valid.\nAllowed program names : \n\t'+'\n\t'.join(appPath.keys()))
					else:
						appPath[app] = path
		f.close()
		return appPath


def concatFile(listFiles, output):
	"""
		Concatenate all the file in a list.

		:param listFiles: A list containing the path of files to concatenate.
		:type listFiles: list
		:param output: The output file.
		:type output: str
		:return: void
	"""

	with open(output,'wb') as o:
		for f in listFiles:
			with open(f,'rb') as fd:
				shutil.copyfileobj(fd, o, 1024*1024*10)


def extractArchive(inFile, outFile):
	"""
		extract an .gz or .bz2 archive
	"""

	if '.gz' in (inFile):
		i = gzip.open(inFile, 'rb')
	elif '.bz2' in (inFile):
		i = bz2.BZ2File(inFile, 'rb')
	else:
		raise IOError("Unsupported archive format. Please use .gz or .bz2 files.")
	o =	open(outFile, 'w')
	for line in i:
		if type(line) is str:
			o.write(line)
		elif type(line) is bytes:
			o.write(line.decode('utf_8', 'ignore'))
	i.close()
	o.close()



def __main__():

	timeStart = datetime.datetime.now()
	parser = argparse.ArgumentParser(description='Pipeline for taxonomic assignment of metatranscriptomic data.')
	parser.add_argument( '-1', '--q1', required=True, help='The first input fasta/fastq file.')
	parser.add_argument( '-2', '--q2', help='The second input fasta/fastq file (Don\'t fill this option if you have single reads).')
	parser.add_argument( '-f', '--format', default='fasta', help='The format of your reads ("fasta" or "fastq"). (default: %(default)s)')
	parser.add_argument( '-c', '--config', default='appPaths.conf', help='The file containing the path of each programs. (default: %(default)s)')
	parser.add_argument( '-t', '--thread', default=1, help='Number of threads tu use. (default: %(default)s)')
	parser.add_argument( '-b', '--evalueB', default=0.00001, help='The evalue for the blast analysis (default: %(default)s).')
	parser.add_argument( '-u', '--evalueU', default=0.0001, help='The evalue for the usearch analysis. (default: %(default)s)')
	parser.add_argument( '-a', '--accel', default=0.8, help='The accel parameter for Usearch. (default: %(default)s)')
	parser.add_argument( '-n', '--nucBank', required=True, help='The path to the viral nucleotidic blast database.')
	parser.add_argument( '-p', '--protBank', required=True, help='The path to the viral proteic blast database.')
	parser.add_argument( '-l', '--blastlengthmin', default=0, help='Minimum length of the blast hit. (default: %(default)s)')
	parser.add_argument( '-k', '--kmersize', default=31, help='The kmer size for the assembly step. (default: %(default)s)')
	parser.add_argument( '-m', '--meganLicence', required=True, help='Path to the megan licence file.')
	parser.add_argument( '-r', '--ribosome', required=True, help='Path to the ribosome fasta file.')
	parser.add_argument( '-i', '--indexRibosome', required=True, help='Path to the index of ribosome fasta file.')
	parser.add_argument( '-g', '--tax', required=True, help='Path to the taxonomy dictionnary. (GenBank id | taxID)')
	options = parser.parse_args()

	# checking options
	filesNotFound = []
	for f in [options.q1, options.meganLicence, options.ribosome, options.tax, options.config]:
		if not checkFileExist(f):
			filesNotFound.append(f)
	if filesNotFound:
		raise IOError("The following files are not found :\n\t"+'\n\t'.join(filesNotFound))

	if options.format != 'fasta' and options.format != 'fastq':
		raise ValueError('Invalid --format option. Please fill "fasta" or "fastq"')
	
	appPath = parseAppPaths(options.config)
	nbThreads = int(options.thread)
	evalueB = float(options.evalueB)
	evalueU = float(options.evalueU)
	accel = float(options.accel)
	blastlengthmin = int(options.blastlengthmin)
	kmersize = int(options.kmersize)
	scriptsPath = os.path.dirname(os.path.abspath(__file__))

	if '.gz' in options.q1 or '.bz2' in options.q1:
		print ('Extract the reads file(s)')
		extractArchive(options.q1, 'inputReads1.fastq')
		reads1File = 'inputReads1.fastq'
		if options.q2:
			extractArchive(options.q2, 'inputReads2.fastq')
			reads2File = 'inputReads2.fastq'
		print ('Done')
	else:
		reads1File = options.q1

	if options.q2:
		print ("\n\nConcatenation of the two reads files...")
		sys.stdout.flush()
		concatFile(reads1File, reads2File, 'reads.fq')
		readsFile = "reads.fq"
		print ("Done.")
		sys.stdout.flush()
	else:
		readsFile = reads1File
	
	print ("\n\nSort the RNA reads with SorteMeRNA...")
	sys.stdout.flush()
	cmd = "%s --ref %s,%s --reads %s --aligned reads_aligned --other reads_rejected --fastx -a %s" % (appPath['sortmerna'], options.ribosome, options.indexRibosome, readsFile, nbThreads)
	run_job(getframeinfo(currentframe()), cmd, "Error in sortmerna.")
	print ("Done.")
	
	print ("\n\nAssembly with Ray...")
	sys.stdout.flush()
	cmd = "%s -s reads_rejected.%s -k %s -disable-scaffolder -o RayOutput" % (appPath['ray'], options.format, kmersize)
	run_job(getframeinfo(currentframe()), cmd, "Error in ray.")
	print ("Done.")
	
	print ("\n\nMapping with bwa...")
	print ("\nIndex the contigs...")
	sys.stdout.flush()
	cmd = "%s index RayOutput/Contigs.fasta" % (appPath['bwa'])
	run_job(getframeinfo(currentframe()), cmd, "Error in bwa index.")
	print ("Done.")

	print ("Construct the SA...")
	sys.stdout.flush()
	cmd = "%s aln RayOutput/Contigs.fasta -t %s -f outputBWA.sai reads_rejected.%s" % (appPath['bwa'], nbThreads, options.format)
	run_job(getframeinfo(currentframe()), cmd, "Error in bwa aln.")
	print ("Done.")

	print ("Map the reads on contigs...")
	cmd = "%s samse -f output.sam RayOutput/Contigs.fasta outputBWA.sai reads_rejected.%s" % (appPath['bwa'], options.format)
	run_job(getframeinfo(currentframe()), cmd, "Error in bwa samse.")
	print ("Done.")
	print ("Mapping finished.")
	cmd = 'ls -R'
	run_job(getframeinfo(currentframe()), cmd, "Error in ls.")
	print ("\n\nRetrieve no mapped reads...")
	sys.stdout.flush()
	cmd = "python "+scriptsPath+"/bin/SortUnmappedReads.py output.sam"
	run_job(getframeinfo(currentframe()), cmd, "Error in SortUnmappedReads.py.")
	print ("Done.")

	print ("\n\nConcatenate contigs and singletons...")
	sys.stdout.flush()
	concatFile(['RayOutput/Contigs.fasta', 'unmappedReads.fasta'], "contigs_and_unmapped.fasta")
	print ("Done.")

	print ("\n\nBlast the contigs and unmapped reads on viral nucleiotidic database...")
	sys.stdout.flush()
	cmd = "%s -db %s -query contigs_and_unmapped.fasta -outfmt 6 -max_target_seqs 1 -num_threads %s -evalue %s -out blastn_output" % (appPath['blastn'], options.nucBank, nbThreads, evalueB)
	run_job(getframeinfo(currentframe()), cmd, "Error in Blast.")
	print ("Done.")

	cmd = "ls -R"
	run_job(getframeinfo(currentframe()), cmd, "Error in cmd.")
	
	print ("\n\nRetrieve the unblasted sequences...")
	sys.stdout.flush()
	cmd = "python "+scriptsPath+"/bin/selectNoBlastHit.py contigs_and_unmapped.fasta blastn_output"
	run_job(getframeinfo(currentframe()), cmd, "Error in selectNoBlastHit.py.")
	print ("Done.")

	print ("\n\nAlign the unblasted sequence on viral proteic database with usearch...")
	sys.stdout.flush()
	cmd="%s -ublast contigs_and_unmapped_noblasthit.fasta -db %s -evalue %s -accel %s -threads %s -blast6out outputUblast_reads_rejected" % (appPath['usearch'], options.protBank, evalueU, accel, nbThreads)
	# cmd="blastx -db %s -query contigs_and_unmapped_noblasthit.fasta -evalue %s -outfmt 6 -max_target_seqs 1 -num_threads %s -out outputUblast_reads_rejected" % (options.protBank, evalueB, nbThreads)
	run_job(getframeinfo(currentframe()), cmd, "Error in usearch.")
	print ("Done.")

	print ("\n\nConcatenate the both outputs (blastn and usearch)...")
	sys.stdout.flush()
	cmd = "python "+scriptsPath+"/bin/ConcatBlastOutput.py -l %s -o output_Blast_and_Usearch -1 blastn_output -2 outputUblast_reads_rejected" % (blastlengthmin)
	run_job(getframeinfo(currentframe()), cmd, "Error in ConcatBlastOutput.py.")
	print ("Done.")

	print ("\n\nSelect only the interesting part of the sequences corresponding to the exact alignment...")
	sys.stdout.flush()
	cmd = "python "+scriptsPath+"/bin/selectReads.py -r contigs_and_unmapped.fasta -b output_Blast_and_Usearch"
	run_job(getframeinfo(currentframe()), cmd, "Error in selectReads.py.")
	print ("Done.")

	print ("\n\nCount the reads by contigs...")
	sys.stdout.flush()
	cmd = "python "+scriptsPath+"/bin/ReadsByContigs.py output.sam"
	run_job(getframeinfo(currentframe()), cmd, "Error in ReadsByContigs.py.", True)
	print ("Done.")

	print ("\n\nPrepare the Megan files...")
	sys.stdout.flush()
	try:
		os.makedirs("OutputMegan/FichiersDSV/")
		os.makedirs("OutputMegan/FichierSauvegarde/")
		os.makedirs("OutputMegan/Graphiques/")
		os.makedirs("OutputMegan/ReadsByGenus/")
	except Exception as e:
		print(e)
	cmd = "python "+scriptsPath+"/bin/CreateMeganDSV.py -l %s -b output_Blast_and_Usearch -t %s -r ReadsByContigs" % (blastlengthmin, options.tax)
	run_job(getframeinfo(currentframe()), cmd, "Error in CreateMeganDSV.py.", True)
	print ("Done.")

	print ("\n\nRun MEGAN...")
	sys.stdout.flush()
	cmd = "%s --auto-servernum --server-num=1 %s -g -L %s -c %s " % (appPath['xvfb'], appPath['megan'], options.meganLicence, "bin/MeganInputScript")
	
	run_job(getframeinfo(currentframe()), cmd, "Error in CreateMeganDSV.py.", True)
	print ("Done.")

	print ("\n\nPipeline is over.")
	print("total time : "+str(datetime.datetime.now() - timeStart))

	return 0

if __name__ == "__main__": __main__()