#!/bin/sh

############      SGE CONFIGURATION      ###################
# write error messages in the error log file
#$ -j y  

# shell to use 
#$ -S /bin/bash 

#$ -pe ompi 12 # the number of threads to use

# Queue
#$ -q bioinfo.q

#$ -V

# Job name
#$ -N metatax
############################################################
 
path_to_reads_dir="/data/projects/metagenome/ReadsBruts"
path_to_scripts="/data/projects/metagenome/metatax"
path_to_data="/data/projects/metagenome/data"
path_to_result="/data/projects/metagenome/metataxResults/$JOB_NAME-$JOB_ID/"
path_to_tmp="/scratch/$JOB_NAME-$JOB_ID"
# path_to_reads="/data/projects/metagenome/metataxResults/Metatax-573583/inputReads1.fastq"
 
###### Creation of directories
mkdir $path_to_tmp
mkdir $path_to_tmp/data

###### Copy the files to the scratch space
if [ -z $q1 ]; then
	echo "Please enter the path to the reads file."
	exit 128
fi
echo "Transfert data on the /scratch space."
scp -rp $path_to_reads_dir/$q1 $path_to_tmp/

file1=$path_to_reads_dir/$q1
reads1filename="${file1##*/}"


if [ ! -z $q2 ]; then
	scp -rp $path_to_reads_dir/$q2 $path_to_tmp/
	file2=$path_to_reads_dir/$q2
	reads2filename="${file2##*/}"
fi

scp -rp /$path_to_scripts/* $path_to_tmp/
scp -rp /$path_to_data $path_to_tmp/
# scp -rp /$path_to_reads $path_to_tmp/

# change the working directory and give the executional rights to the script
cd $path_to_tmp
chmod -R 755 ./*.py
chmod -R 755 ./bin/*.py

# prepare the arguments for the runMetatax.py script
if [ -z $format ]; then
	readsFormat='fasta'
else
	readsFormat=$format
fi

appPath='./appPaths.conf'
nucBank='./data/gbvrl'
protBank='./data/gbvrlProt.udb'
# protBank='/data/projects/banks/GBvrlProt'
ribosome='./data/rRNA_AEDES/ribosomes_aedes.fasta'
indexRibosome='./data/rRNA_AEDES/index_rRNAaedes'
meganLicence='./data/MEGAN5-academic-license.txt'
taxonomy='./data/taxonomyGI'

###### Execute the pipeline
if [ ! -z $q2 ]; then
	metatax="python3.3 ./runMetatax.py --q1 $reads1filename --q2 $reads2filename --format $readsFormat --config $appPath --thread $NSLOTS --nucBank $nucBank --protBank $protBank --ribosome $ribosome --indexRibosome $indexRibosome--tax $taxonomy  --meganLicence $meganLicence"
else
	metatax="python3.3 ./runMetatax.py --q1 $reads1filename --format $readsFormat --config $appPath --thread $NSLOTS --nucBank $nucBank --protBank $protBank --ribosome $ribosome --indexRibosome $indexRibosome --tax $taxonomy --meganLicence $meganLicence"
fi
# metatax="python3.3 ./runMetatax.py -h"
echo "Running Metatax :"
echo "$metatax"
$metatax
##### Trsansfert the result files to the nas2
echo "create result folder"
mkdir $path_to_result

# scp -rp $path_to_tmp/inputReads1.fastq $path_to_result

listFiles="$path_to_tmp/output_Blast_and_Usearch_Tax $path_to_tmp/contigs_and_unmapped_SeqSelected.fasta $path_to_tmp/OutputMegan/ $path_to_tmp/RayOutput/ $path_to_tmp/reads_rejected.$readsFormat $path_to_tmp/ReadsByContigs"
echo "copy rslt file"
for file in $listFiles    
do
	scp -rp $file $path_to_result
done

echo "Transfert donnees node -> master"

#### Remove the files in the /scratch space
rm -rf $path_to_tmp
echo "Suppression des donnees sur le noeud"